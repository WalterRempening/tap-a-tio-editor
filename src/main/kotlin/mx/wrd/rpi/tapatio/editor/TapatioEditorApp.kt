package mx.wrd.rpi.tapatio.editor

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties
class TapatioEditorApp


fun main(args: Array<String>) {
    runApplication<TapatioEditorApp>(*args)
}